### Secure Nginx with Let's Encrypt on Ubuntu 22.04
```bash
#install certbot
sudo snap install core; sudo snap refresh core
#if your server have installed the previous version
sudo apt remove certbot
#install
sudo snap install --classic certbot
#link the certbot command from the snap install directory to your path
sudo ln -s /snap/bin/certbot /usr/bin/certbot

#point nginx to domain with hosting uptime-kuma (domain_name: monitor.dalin.host)
#uptime-kuma is running on http://174.138.30.206:3001, container 3001:3001
cd /etc/nginx/conf.d
sudo vi monitor.dalin.host.conf
#paste the script below to that file, save and exit
server {
        listen 80;
        listen [::]:80;
        #root /var/www/html;
        # Add index.php to the list if you are using PHP
        # index index.html index.htm index.nginx-debian.html;

        server_name domain_name;

        location / {
                # First attempt to serve request as file, then
                # as directory, then fall back to displaying a 404.
                #try_files $uri $uri/ =404;
                proxy_pass http://localhost:3001;
                proxy_http_version 1.1;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection "upgrade";
                proxy_set_header Host $host;

        }
}
sudo nginx -t
service nginx restart
#obtaining as SSL Certificate to domain_name
sudo certbot --nginx -d domain_name
#insert email, Y and Y
```